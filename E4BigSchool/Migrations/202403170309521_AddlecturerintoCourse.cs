﻿namespace E4BigSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddlecturerintoCourse : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Courses", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Courses", new[] { "User_Id" });
            RenameColumn(table: "dbo.Courses", name: "User_Id", newName: "LecturerId");
            AlterColumn("dbo.Courses", "LecturerId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Courses", "LecturerId");
            AddForeignKey("dbo.Courses", "LecturerId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.Courses", "LectureId");
            DropColumn("dbo.Courses", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "UserId", c => c.Byte(nullable: false));
            AddColumn("dbo.Courses", "LectureId", c => c.String(nullable: false));
            DropForeignKey("dbo.Courses", "LecturerId", "dbo.AspNetUsers");
            DropIndex("dbo.Courses", new[] { "LecturerId" });
            AlterColumn("dbo.Courses", "LecturerId", c => c.String(maxLength: 128));
            RenameColumn(table: "dbo.Courses", name: "LecturerId", newName: "User_Id");
            CreateIndex("dbo.Courses", "User_Id");
            AddForeignKey("dbo.Courses", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
